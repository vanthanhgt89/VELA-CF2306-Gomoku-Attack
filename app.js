const generate_Matrix = (n, init) => {
  let mat = [];
  for (let i = 0; i < n; i++) {
    a = [];
    for (let j = 0; j < n; j++) {
      a[j] = init;
    }
    mat[i] = a;
  }
  return mat;
}



const Horizontal = (Mat, Cur_row, Cur_col, Value) => {
  //di sang ben trai
  let count_left = 0;
  let count_right = 0;
  //Di sang phia ben trai so voi vi tri hien tai
  for (let i = Cur_col; i >= 0; i--) {
    if (Mat[Cur_row][i] === Value) {
      count_left++;
    }
    else {
      break;
    }
  }
  //Di sang phia ben phai so voi vi tri hien tai
  for (let j = Cur_col + 1; j < 100; j++) {
    if (Mat[Cur_row][j] === Value) {
      count_right++;
    }
    else {
      break;
    }
  }
  if (count_right + count_left >= 5) {
    return 1;
  }else {
    return 'test'
  }
}

const Vertical = (Mat, Cur_row, Cur_col, Value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  //Di len tren so voi vi tri hien tai
  for (let i = Cur_row; i >= 0; i--) {
    if (Mat[i][Cur_col] === Value) {
      count_up++;
    }
    else {
      break;
    }
  }
  //Di xuong duoi
  for (let j = Cur_row + 1; j < 100; j++) {
    if (Mat[j][Cur_col] === Value) {
      count_down++;
    }
    else {
      break;
    }
  }
  if (count_right + count_left >= 5) {
    return 1;
  }
}

const Diagonal = (Mat, Cur_row, Cur_col, Value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  let tempRow = Cur_row;
  let tempCol = Cur_col;
  //Di cheo len tren ben trai so voi vi tri hien tai
  for (let i = Cur_row; i >= 0; i--) {
    if (Mat[i][tempCol] === Value) {
      tempCol--;
      count_up++;
    }
    else {
      break;
    }
  }
  tempCol = Cur_col;
  //Di cheo xuong duoi ben phai
  for (let j = Cur_row + 1; j < 100; j++) {
    if (Mat[j][tempCol] === Value) {
      count_down++;
      tempCol++;
    }
    else {
      break;
    }
  }
  if (count_up + count_down >= 5) {
    return 1;
  }
}

const Diagonal_main = (Mat, Cur_row, Cur_col, Value) => {
  //di len tren
  let count_up = 0;
  let count_down = 0;
  let tempCol = Cur_col;
  let tempRow = Cur_row;
  //Di cheo len ben phai
  for (let i = Cur_row; i >= 0; i--) {
    if (Mat[i][tempCol] === Value) {
      count_up++;
      tempCol++;
    }
    else {
      break;
    }
  }
  tempCol = Cur_col
  //Di xuong duoi
  for (let j = Cur_row + 1; j < 100; j++) {
    if (Mat[j][tempCol] === Value) {
      count_down++;
      tempCol--;
    }
    else {
      break;
    }
  }
  if (count_right + count_left >= 5) {
    return 1;
  }
}

module.exports = {
  generate_Matrix,
  Horizontal,
  Vertical,
  Diagonal,
  Diagonal_main

}